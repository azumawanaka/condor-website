        

        </main>
        <!-- <?php if(uri_string() != "contact") : ?>
        <section class="wrp sec-talk">
        	<div class="cntr">
        		<h3>Are you looking to expand your business to the Philippines?</h3>
        	</div>
        </section>
    	<?php endif; ?> -->
        <footer>
			<div class="footer-cntr">
				<div class="tc">
					<a href="<?=base_url("contact");?>" class="btn btn-touch">
						<span>Talk to Us</span>
					</a>
				</div>
				<div class="cntr-body dflex">
					<div class="left_part">
						<a href="#" class="logo">
							<img src="<?=base_url();?>/assets/img/logo-white.svg" alt="" class="is-wide">
						</a>

						<div class="ftr-add">
							<p>
								<strong>Headquarters</strong><br>
								Cebu Park Centrale 5F Jose Maria Del Mar St.,<br>
								Asiatown IT Park, Cebu City 6000<br>
								<a href="tel:+634 3494 9579">+634 3494 9579</a>
							</p>
						</div>	
					</div>
					<div class="right_part">
						<div class="col-links">
							<div class="link-item unique-size">
								<ul class="gnav-list">
									<li>
						                <a href="#">Custom Tech Fit-Out </a>
						            </li>
						            <li>
						                <a href="#">Technology Suites</a>
						            </li>
						            <li>
						                <a href="#">Advance Solutions</a>
						            </li>
						            <li>
						                <a href="#">Retrofitting</a>
						            </li>
								</ul>
							</div>
							<div class="link-item unique-size is-v2">
								<ul class="gnav-list">
									<li>
						                <a href="#">Integration Infrastructure</a>
						            </li>
						            <li>
						                <a href="#">Transcend with technology</a>
						            </li>
						            <li>
						                <a href="#">Design & Development</a>
						            </li>
								</ul>
							</div>
							<div class="link-item unique-size is-v3">
								<ul class="gnav-list">
									<li>
						                <a href="#">Sustainability</a>
						            </li>
						            <li>
						                <a href="<?=base_url("about");?>">About ASP</a>
						            </li>
						            <li>
						                <a href="<?php echo base_url("enterprise"); ?>" class="<?php if(uri_string() == "enterprise") { echo "is-active"; } ?>">Enterprise</a>
						            </li>
								</ul>
							</div>
							<div class="link-item unique-size is-v3">
								<ul class="gnav-list">
						            <!-- <li>
						                <a href="<?php echo base_url("ideas"); ?>" class="<?php if(uri_string() == "ideas") { echo "is-active"; } ?>">Ideas</a>
						            </li> -->
						            <li>
						                <a href="<?php echo base_url("services"); ?>" class="<?php if(uri_string() == "services") { echo "is-active"; } ?>">Services</a>
						            </li>
						            <li>
						                <a href="<?php echo base_url("contact"); ?>" class="<?php if(uri_string() == "contact") { echo "is-active"; } ?>">Contact Us</a>
						            </li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="ftr-bottom">
					<div class="other-links">
						<ul>
							<li>
								<a href="#">Privacy Policy </a>
							</li>
							<li>
								<a href="#">Cookie Policy</a>
							</li>
							<li>
								<a href="#">Terms of use</a>
							</li>
							<li>
								<a href="#">Legal</a>
							</li>
							<li>
								<a href="#">Sitemap</a>
							</li>
						</ul>
					</div>
					<div class="copy-right">
						<p>Copyright &copy; <?php echo date("Y", strtotime("now")); ?> ASP Inc. All rights reserved.</p>
					</div>
				</div>
			</div>
        </footer>

    </body>
    <!-- scripts -->
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="<?=base_url('assets/js/lib/owl-carousel.js');?>"></script>

  	<!-- Core plugin JavaScript-->
    <script src="<?=base_url('assets/js/main.js');?>"></script>
    <!-- //scripts -->
    

    <script>
      AOS.init();
    </script>
    <script>
      $(function(){
        var hero_slier = $("#hero-slider")
            hero_slier.owlCarousel({
            lazyLoad: true,
            center: false,
            items: 1,
            nav: false,
            margin: 0,
            padding: 0,
            autoplay: 5000,
            touchDrag: false,
            pullDrag: false,
            mouseDrag: false,
            loop: true,
            dots: false,
            smartSpeed: 1000,
			animateOut: 'fadeOut',
            autoplayHoverPause: false
		})

        var slogan_slider = $("#slogan-slider")
			slogan_slider.owlCarousel({
            lazyLoad: true,
            center: false,
            items: 1,
            nav: false,
            margin: 0,
            padding: 0,
            autoplay: true,
            touchDrag: true,
            pullDrag: true,
            mouseDrag: true,
            loop: true,
            dots: true,
            smartSpeed: 1000,
			// animateOut: 'fadeOut',
            autoplayHoverPause: false
        })

		var m_slider = $("#m-slider")
			m_slider.owlCarousel({
				lazyLoad: true,
				center: false,
				items: 1,
				nav: false,
				margin: 0,
				padding: 0,
				autoplay: 3000,
				touchDrag: false,
				pullDrag: false,
				mouseDrag: true,
				loop: true,
				dots: false,
				animateOut: 'fadeOut',
				smartSpeed: 500,
				autoplayHoverPause: false
			})

		var e_slider2 = $("#e-slider2")
			e_slider2.owlCarousel({
				center: false,
				items: 1,
				nav: false,
				margin: 45,
				padding: 0,
				autoplay: 3000,
				touchDrag: true,
				pullDrag: true,
				mouseDrag: true,
				loop: true,
				dots: false,
				smartSpeed: 500,
				autoplayHoverPause: false,
				responsive : {
				    // breakpoint from 0 up
				    0 : {
				        center: false,
				        items: 2
				    },
				    769 : {
				        center: false,
				        items: 7
				    }
				}
			})

		var e_slider3 = $("#enter-slider")
			e_slider3.owlCarousel({
				center: false,
				items: 6,
				nav: true,
				margin: 20,
				padding: 0,
				autoplay: false,
				touchDrag: false,
				pullDrag: false,
				mouseDrag: true,
				loop: true,
				dots: true,
				smartSpeed: 500,
				autoplayHoverPause: false,
				responsive : {
				    0 : {
				        center: true,
				        items: 1
				    },
				    769 : {
				        center: false,
				        items: 2
				    },
				    1000 : {
				        center: false,
				        items: 4
				    }
				}
			})
      })
    </script>

</html>