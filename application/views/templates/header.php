<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title> ASP - <?= $title ?></title>

        <!-- fonts -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- <link href="https://fonts.googleapis.com/css?family=Dosis&display=swap" rel="stylesheet">  -->
         
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,600,700,800,900&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,900&display=swap" rel="stylesheet">
        <!-- stylesheets -->
        <link rel="stylesheet" href="<?=base_url("assets/css/lib/owl-carousel.min.css")?>">
        <link rel="stylesheet" href="<?=base_url("assets/css/lib/animate.css")?>">

        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" href="<?=base_url("assets/css/main.css")?>">
        <!-- //stylesheets -->
        
        <script src="<?=base_url('assets/js/lib/jquery.min.js');?>"></script>
        <script>
            var burl = '<?php echo base_url()?>';
        </script>
    </head>
    <body class="page_<?= strtolower($title) ?>">

        <!-- header -->
        <div class="header-wrp">
            <div class="header-inner">
                <header class="main-header">
                    <a href="<?= base_url(); ?>" class="logo">
                        <img src="<?= base_url(); ?>/assets/img/logo.svg" class="is-wide">
                    </a>
                    <a href="#" id="toggle-nav">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                </header>
                <nav class="main-nav">
                    <ul>
                        <li>
                            <a href="<?=base_url();?>" class="<?php if (!$this->uri->segment(1)) { echo "is-active"; } ?>"><span>home</span></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url("about"); ?>" class="<?php if(uri_string() == "about") { echo "is-active"; } ?>"><span>about</span></a>
                        </li>
                        <li class="has-children">
                            <a href="javascript:;" class="p-a <?php if(uri_string() == "values") { echo "is-active"; } ?>"><span>Delivering Value</span></a>
                            <div class="list-children-box">
                                <ul class="sub-list">
                                    <li>
                                        <a href="#"><span>Solution Architecture</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span>Configuration & Deployment</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span>Investment Efficiency</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span>Industry Insight</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span>Hybrid Cloud</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span>Internet of Things</span></a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="has-children">
                            <a href="javascript:;" class="p-a <?php if(uri_string() == "enterprise") { echo "is-active"; } ?>"><span>enterprise</span></a>
                            <div class="list-children-box">
                                <ul class="sub-list">
                                    <li>
                                        <a href="#"><span>Infrastructure</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span>Smart Building</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span>Security</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span>Networking</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span>Data Management</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span>AI & Analytics</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span>Web Development</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span>Web Hosting</span></a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a href="#" class="<?php if(uri_string() == "products") { echo "is-active"; } ?>"><span>products</span></a>
                        </li>
                        <li>
                            <a href="#" class="<?php if(uri_string() == "pricing") { echo "is-active"; } ?>"><span>pricing</span></a>
                        </li>
                        <li class="li-contact">
                            <a href="<?php echo base_url("contact"); ?>">contact</a>
                        </li>
                    </ul>
                    <!-- <div class="nav-cntct-details">                        
                        <p>Got a Project? Email Us</p>                        
                        <a href="mailto:info@symph.co">info@sample.com</a>                    
                    </div> -->
                </nav>
            </div>
        </div>
        <!-- //header -->
        <main>