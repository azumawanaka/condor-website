<section class="wrp contact-pg">
	<div class="cntr">
		<div class="gap gap-20 gap-0-xs">
			<div class="md-6 xs-12">
				<div class="card-contact_l">
					<h3 class="title">Get in Touch</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ultricies, nisi in maximus volutpat, turpis erat semper est. In leo tortor, lobortis et urna id, lora consequat.</p>
					<ul class="cntct-i">
						<li>
							<img src="<?=base_url();?>/assets/img/ico-global.png">
							121 King Street, Melbourne Victoria 3000 Australia
						</li>
						<li>
							<img src="<?=base_url();?>/assets/img/ico-user.png">
							Call us on:<br>888-123-4587
						</li>
						<li>
							<img src="<?=base_url();?>/assets/img/ico-email.png">
							Email us on:<br>info@example.com
						</li>
					</ul>
				</div>
			</div>
			<div class="md-6 xs-12">
				<div class="card-contact_r">
					<h3 class="title">Drop us a line</h3>
					<!-- form -->
					<form class="cntct-form">
						<div class="form-grp">
							<label>
								<input type="text" name="name" id="name" placeholder="Name">
							</label>
						</div>
						<div class="form-grp">
							<label>
								<input type="email" name="email" id="email" placeholder="Email">
							</label>
						</div>
						<div class="form-grp">
							<label>
								<textarea name="message" id="" placeholder="Message" required=""></textarea>
							</label>
						</div>
						<div class="form-grp tr">
							<button type="submit" class="btn btn-submit">Send Message <i class="material-icons">arrow_forward</i></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>