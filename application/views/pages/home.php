<section class="wrp hero-sec header-fixed">
    <div class="hero-cntr">
        <div id="hero-slider" class="owl-carousel owl-theme">
            <div class="hero-slider-body item">
                <div class="hero-col is-img" style="background: url('<?=base_url();?>/assets/img/slider-01.png') center right / cover no-repeat;">
                </div>
                <div class="hero-col is-info">
                    <div class="hero-title">
                        <h1>
                            <span class="xl">Transcend</span> <span class="xxl">technology</span>
                        </h1>
                        <p>Take your business beyond its existing limits. Implement tools, processes, and strategies that improve the value and performance of your infrastructure.</p>
                    </div>
                </div>
            </div>
            <div class="hero-slider-body item">
                <div class="hero-col is-img" style="background: url('<?=base_url();?>/assets/img/slider-02.png') center right / cover no-repeat;">
                </div>
                <div class="hero-col is-info">
                    <div class="hero-title">
                        <h1>
                            <span class="xl">Transcend</span> <span class="xxl">technology</span>
                        </h1>
                        <p>Take your business beyond its existing limits. Implement tools, processes, and strategies that improve the value and performance of your infrastructure.</p>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="slogans-sec g-pad">
    <div class="cntr">
        <div id="slogan-slider" class="owl-carousel owl-theme tc">
            <div class="item">
                <div class="inner">
                    <h3 class="title">
                        <span>Your Goals</span>
                    </h3>
                    <div class="phar">
                        <p>
                            Whether you’re an established enterprise or a growing startup, discover technologies that transcend the most impactful work of your life and organization.
                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <h3 class="title">
                        <span>Digital Transformation</span>
                    </h3>
                    <div class="phar">
                        <p>
                            Explore the technology solutions that help our clients transcend their infrastructure and their business through digital transformation.
                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <h3 class="title">
                        <span>What's New</span>
                    </h3>
                    <div class="phar">
                        <p>
                            Keep ahead of the latest trends, discover innovative solutions with our thought leadership pieces and stay connected to the latest developments from ASP.
                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <h3 class="title">
                        <span>Enterprise Solutions</span>
                    </h3>
                    <div class="phar">
                        <p>
                            Determine why most of the house hold name companies, trust us to cater their needs in terms of design and building tailored fit solutions for their organization.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="about-sec g-pad">
    <div class="cntr tc">
        <div class="inner">
            <h2 class="line-title">
                Philippine's leading locally owned and operated digital transformation and infrastructure company. Hardware, software, cloud and IoT focus solutions for Visayas and Mindanao.
            </h2>
            <p class="mb">
                Here at ASP Digital Infrastructure, our expertise transcends our customers from discrete processing industries to the next level of digital transformation. Using advance cutting-edge technologies, handpicked very carefully and bundled for greater flexibility and productivity – for companies of any size and any industry.
            </p>
            <p>
                We have a very simple organization that focuses on doing few things well in the IT and Real Estate industry. And focusing is hard in both industry, because focusing does not mean saying yes, it means saying no. So we decided not to do a lot of things, so that we can focus on well curated set of solutions and do it very well.
            </p>
        </div>
    </div>
</section>
<section class="empow-sec g-pad">
    <div class="cntr">
        <div class="inner">
            <h2 class="line-title">
                Empowering great values of technology
            </h2>
            <div class="gap gap-15 gap-0-xs">
                <div class="md-4 xs-12 item">
                    <a href="#" class="card card-services">
                       <!--  <div class="card-header tc">
                            <div class="card-icon"><img src="https://img.icons8.com/cute-clipart/50/000000/web.png"></div>
                        </div> -->
                        <div class="card-body">
                            <div class="card-title tc">
                                <span>Solution Architecture</span>
                            </div>
                            <p class="tc">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo ad eum dicta inventore voluptatum qui neque asperiores.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="md-4 xs-12 item">
                    <a href="#" class="card card-services">
                        <!-- <div class="card-header tc">
                            <div class="card-icon"><img src="https://img.icons8.com/color/48/000000/two-smartphones.png"></div>
                        </div> -->
                        <div class="card-body">
                            <div class="card-title tc">
                                <span>Config. & Deployment</span>
                            </div>
                            <p class="tc">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo ad eum dicta inventore voluptatum qui neque asperiores.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="md-4 xs-12 item">
                    <a href="#" class="card card-services">
                       <!--  <div class="card-header tc">
                            <div class="card-icon"><img src="https://img.icons8.com/cute-clipart/48/000000/accessibility2.png"></div>
                        </div> -->
                        <div class="card-body">
                            <div class="card-title tc">
                                <span>Investment Efficiency</span>
                            </div>
                            <p class="tc">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo ad eum dicta inventore voluptatum qui neque asperiores.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="md-4 xs-12 item">
                    <a href="#" class="card card-services">
                       <!--  <div class="card-header tc">
                            <div class="card-icon"><img src="https://img.icons8.com/nolan/50/000000/module.png"></div>
                        </div> -->
                        <div class="card-body">
                            <div class="card-title tc">
                                <span>Industry Insight</span>
                            </div>
                            <p class="tc">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo ad eum dicta inventore voluptatum qui neque asperiores.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="md-4 xs-12 item">
                    <a href="#" class="card card-services">
                       <!--  <div class="card-header tc">
                            <div class="card-icon"><img src="https://img.icons8.com/cute-clipart/48/000000/accessibility2.png"></div>
                        </div> -->
                        <div class="card-body">
                            <div class="card-title tc">
                                <span>Hybrid Cloud</span>
                            </div>
                            <p class="tc">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo ad eum dicta inventore voluptatum qui neque asperiores.
                            </p>
                        </div>
                    </a>
                </div>
                <div class="md-4 xs-12 item">
                    <a href="#" class="card card-services">
                       <!--  <div class="card-header tc">
                            <div class="card-icon"><img src="https://img.icons8.com/nolan/50/000000/module.png"></div>
                        </div> -->
                        <div class="card-body">
                            <div class="card-title tc">
                                <span>Internet of Things</span>
                            </div>
                            <p class="tc">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quo ad eum dicta inventore voluptatum qui neque asperiores.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="banner-sec">
    <div class="cntr tc g-pad">
        <h2>
            <span class="is-l">Disruption</span> <span class="is-r">Revolution</span>        
        </h2>
    </div>
</section>
<section class="enterprise-sec g-pad">
    <div class="cntr">
        <h2 class="line-title tc">
            Focus enterprise infrustructure solutions
        </h2>
        <div class="gap gap-15 gap-0-xs">
            <div class="md-4 xs-12 item">
                <a href="#" class="card card-enterprise">
                    <div class="card-header">
                        <img src="<?=base_url();?>assets/img/thumb-sample.jpg" class="is-wide">
                    </div>
                    <div class="card-body">
                        <h3>
                            <span>Infrastructure</span>
                        </h3>
                        <!-- <p class="more">Learn More</p> -->
                    </div>
                </a>
            </div>
            <div class="md-4 xs-12 item">
                <a href="#" class="card card-enterprise">
                    <div class="card-header">
                        <img src="<?=base_url();?>assets/img/thumb-sample.jpg" class="is-wide">
                    </div>
                    <div class="card-body">
                        <h3>
                            <span>Security</span>
                        </h3>
                        <!-- <p class="more">Learn More</p> -->
                    </div>
                </a>
            </div>
            <div class="md-4 xs-12 item">
                <a href="#" class="card card-enterprise">
                    <div class="card-header">
                        <img src="<?=base_url();?>assets/img/thumb-sample.jpg" class="is-wide">
                    </div>
                    <div class="card-body">
                        <h3>
                            <span>Networking</span>
                        </h3>
                        <!-- <p class="more">Learn More</p> -->
                    </div>
                </a>
            </div>
            <div class="md-4 xs-12 item">
                <a href="#" class="card card-enterprise">
                    <div class="card-header">
                        <img src="<?=base_url();?>assets/img/thumb-sample.jpg" class="is-wide">
                    </div>
                    <div class="card-body">
                        <h3>
                            <span>Data Management</span>
                        </h3>
                        <!-- <p class="more">Learn More</p> -->
                    </div>
                </a>
            </div>
            <div class="md-4 xs-12 item">
                <a href="#" class="card card-enterprise">
                    <div class="card-header">
                        <img src="<?=base_url();?>assets/img/thumb-sample.jpg" class="is-wide">
                    </div>
                    <div class="card-body">
                        <h3>
                            <span>AI & Analytics</span>
                        </h3>
                        <!-- <p class="more">Learn More</p> -->
                    </div>
                </a>
            </div>
            <div class="md-4 xs-12 item">
                <a href="#" class="card card-enterprise">
                    <div class="card-header">
                        <img src="<?=base_url();?>assets/img/thumb-sample.jpg" class="is-wide">
                    </div>
                    <div class="card-body">
                        <h3>
                            <span>Web Development</span>
                        </h3>
                        <!-- <p class="more">Learn More</p> -->
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<secftion class="brands-sec dblock wrp">
    <div class="cntr">
        <div id="e-slider2" class="owl-carousel owl-theme">
            <div class="item">
                <a href="https://cyberinc.com/" class="e-thumbs" target="_blank">
                    <img class="is-wide" src="<?=base_url();?>/assets/img/brands/cyberinc.png">
                </a>
            </div>
            <div class="item">
                <a href="https://www.delltechnologies.com/en-ph/index.htm" class="e-thumbs" target="_blank">
                    <img class="is-wide" src="<?=base_url();?>/assets/img/brands/dell.png">
                </a>
            </div>
            <div class="item">
                <a href="https://www.hpe.com/us/en/home.html" class="e-thumbs" target="_blank">
                    <img class="is-wide" src="<?=base_url();?>/assets/img/brands/hewlett.png">
                </a>
            </div>
            <div class="item">
                <a href="https://www.ibm.com/ph-en" class="e-thumbs" target="_blank">
                    <img class="is-wide" src="<?=base_url();?>/assets/img/brands/ibm.png">
                </a>
            </div>
            <div class="item">
                <a href="https://www.lenovo.com/ph/en/pc?orgRef=%2F%2Fwww.google.com%2F" class="e-thumbs" target="_blank">
                    <img class="is-wide" src="<?=base_url();?>/assets/img/brands/lenovo.png">
                </a>
            </div>
            <div class="item">
                <a href="https://www.vmware.com/" class="e-thumbs" target="_blank">
                    <img class="is-wide" src="<?=base_url();?>/assets/img/brands/vmware.jpeg">
                </a>
            </div>
            <div class="item">
                <a href="https://www.sas.com/en_ph/home.html" class="e-thumbs" target="_blank">
                    <img class="is-wide" src="<?=base_url();?>/assets/img/brands/sas.jpeg">
                </a>
            </div>
            <div class="item">
                <a href="https://www.informatica.com/#fbid=qjexjX7rFun" class="e-thumbs" target="_blank">
                    <img class="is-wide" src="<?=base_url();?>/assets/img/brands/informatica.png">
                </a>
            </div>
        </div>
    </div>
</secftion>
