<section class="wrp pg-mission">
    <div class="cntr header-fixed" data-aos="fade-up" data-aos-duration="800">
        <h2 class="lg-title">Our Mission</h2>
        <div class="txt-box tc">
            <p>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Reiciendis, repellendus! Eligendi voluptatem rerum odio maxime omnis sed mollitia vero aspernatur odit!
            </p>
        </div>
    </div>
    <!-- fadeIn animation -->
    <div class="ms-wrp" data-aos="fade" data-aos-duration="1000">
        <div id="m-slider" class="owl-theme owl-carousel">
            <div class="slide-item owl-lazy" data-src="<?=base_url();?>/assets/img/mission-01.jpg"></div>
            <div class="slide-item owl-lazy" data-src="<?=base_url();?>/assets/img/mission-02.jpg"></div>
            <div class="slide-item owl-lazy" data-src="<?=base_url();?>/assets/img/mission-03.jpg"></div>
        </div>
    </div>
    <!-- //fadein -->
</section>
<section class="wrp m_other-inf">
    <div class="cntr">
        <div class="other_box-inf">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
        </div>
    </div>
</section>

<section class="wrp m_col-sec">
    <div class="cntr">
        <h2 class="main_title tc">
            <span>Lorem Ipsum Dolor</span>
        </h2>
        <div class="txt-box tc">
            <p>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Reiciendis, repellendus! Eligendi voluptatem rerum odio maxime omnis sed mollitia vero aspernatur odit!
            </p>
        </div>
        <div class="gap gap-20 gap-10-sm gap-0-xs body_cols">
            <div class="md-3 sm-6 xs-12 mb-0 mb-30-xs">
                <div class="card card-mission">
                    <div class="card-header">
                        <div class="card-img">
                            <img src="https://img.icons8.com/color/48/000000/trust.png">
                        </div>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title">Sample</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat.
                        </p>
                    </div>
                </div>
            </div>
            <div class="md-3 sm-6 xs-12 mb-0 mb-30-xs">
                <div class="card card-mission">
                    <div class="card-header">
                        <div class="card-img">
                            <img src="https://img.icons8.com/color/48/000000/change.png">
                        </div>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title">Sample</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat.
                        </p>
                    </div>
                </div>
            </div>
            <div class="md-3 sm-6 xs-12 mb-0 mb-30-xs">
                <div class="card card-mission">
                    <div class="card-header">
                        <div class="card-img">
                            <img src="https://img.icons8.com/color/48/000000/hacking.png">
                        </div>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title">Sample</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat.
                        </p>
                    </div>
                </div>
            </div>
            <div class="md-3 sm-6 xs-12">
                <div class="card card-mission">
                    <div class="card-header">
                        <div class="card-img">
                            <img src="https://img.icons8.com/color/48/000000/teamwork.png">
                        </div>
                    </div>
                    <div class="card-body">
                        <h3 class="card-title">Sample</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>